﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QualificationControl
{
    public class Mdb
    {
        public OleDbConnection g_connection;
        /// <summary>
        /// Установить соединение
        /// </summary>
        /// <param name="path"></param>
        public void DoConnected(string path)
        {
            try
            {
                g_connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"" +
                                 path + "\"; Jet OLEDB:Database Password=\"\";" +
                                 "Jet OLEDB:Engine Type=5;Jet OLEDB:Encrypt Database=True;");
                g_connection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Произошла ошибка в момент установления соединения с бд: " + ex.Message);
            }
        }

        public void DoCertifited(int id, DateTime newCertify, DateTime nextCertify)
        {
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE Workers SET last_certify=@last_certify, next_certify=@next_certify WHERE Id=@Id";
                cmd.Parameters.AddWithValue("last_certify", newCertify.ToString("G"));
                cmd.Parameters.AddWithValue("next_certify", nextCertify.ToString("G"));
                cmd.Parameters.AddWithValue("Id", id);
                cmd.ExecuteNonQuery();
            }
        }
        public Workers GetWorkerById(int id)
        {
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM Workers WHERE Id=@Id";
                cmd.Parameters.AddWithValue("Id", id);
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    if(reader.Read())
                    {
                        Workers workers = new Workers();
                        workers.Id = reader.GetInt32(0);
                        workers.LastName = reader.GetString(1);
                        workers.FirstName = reader.GetString(2);
                        workers.MiddleName = reader.GetString(3);
                        workers.LastCertify = reader.GetDateTime(4);
                        workers.NextCertify = reader.GetDateTime(5);
                        return workers;
                    }
                }
            }
            return null;
        }

        public void DoChangeWorkerProps(Workers worker)
        {
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = @"UPDATE Workers SET 
                        last_name=@last_name, first_name=@first_name, middle_name=@middle_name,
                        last_certify=@last_certify, next_certify=@next_certify WHERE Id=@Id";
                cmd.Parameters.AddWithValue("last_name", worker.LastName);
                cmd.Parameters.AddWithValue("first_name", worker.FirstName);
                cmd.Parameters.AddWithValue("middle_name", worker.MiddleName);
                cmd.Parameters.AddWithValue("last_certify", worker.LastCertify.ToString("G"));
                cmd.Parameters.AddWithValue("next_certify", worker.NextCertify.ToString("G"));
                cmd.Parameters.AddWithValue("Id", worker.Id);
                cmd.ExecuteNonQuery();
            }
        }

        public void DoCreateWorkers(Workers workers)
        {
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = @"INSERT INTO Workers (last_name, first_name, middle_name, last_certify, next_certify)
                                    VALUES (?,?,?,?,?)";
                cmd.Parameters.AddWithValue("last_name", workers.LastName);
                cmd.Parameters.AddWithValue("first_name", workers.FirstName);
                cmd.Parameters.AddWithValue("middle_name", workers.MiddleName);
                cmd.Parameters.AddWithValue("last_certify", workers.LastCertify.ToString("G"));
                cmd.Parameters.AddWithValue("next_certify", workers.NextCertify.ToString("G"));
                cmd.ExecuteNonQuery();
            }
        }

        public List<Workers> GetWorkersList()
        {
            List<Workers> workerList = new List<Workers>();
            using(OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM Workers";
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        Workers workers = new Workers();
                        workers.Id = reader.GetInt32(0);
                        workers.LastName = reader.GetString(1);
                        workers.FirstName = reader.GetString(2);
                        workers.MiddleName = reader.GetString(3);
                        workers.LastCertify = reader.GetDateTime(4);
                        workers.NextCertify = reader.GetDateTime(5);
                        workerList.Add(workers);
                    }
                }
            }
            return workerList;
        }
    }
}
