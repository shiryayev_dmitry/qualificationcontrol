﻿namespace QualificationControl
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvWorkers = new System.Windows.Forms.DataGridView();
            this.btnCertify = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MiddleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastQualification = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NextAttestation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeLeft = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkers)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvWorkers
            // 
            this.dgvWorkers.AllowUserToAddRows = false;
            this.dgvWorkers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvWorkers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvWorkers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWorkers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.LastName,
            this.FirstName,
            this.MiddleName,
            this.LastQualification,
            this.NextAttestation,
            this.timeLeft});
            this.dgvWorkers.Location = new System.Drawing.Point(12, 38);
            this.dgvWorkers.Name = "dgvWorkers";
            this.dgvWorkers.RowHeadersVisible = false;
            this.dgvWorkers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvWorkers.Size = new System.Drawing.Size(776, 400);
            this.dgvWorkers.TabIndex = 0;
            this.dgvWorkers.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvWorkers_CellContentDoubleClick);
            // 
            // btnCertify
            // 
            this.btnCertify.Location = new System.Drawing.Point(572, 9);
            this.btnCertify.Name = "btnCertify";
            this.btnCertify.Size = new System.Drawing.Size(134, 23);
            this.btnCertify.TabIndex = 2;
            this.btnCertify.Text = "Провести аттестацию";
            this.btnCertify.UseVisualStyleBackColor = true;
            this.btnCertify.Click += new System.EventHandler(this.btnCertify_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(434, 9);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(132, 23);
            this.btnCreate.TabIndex = 3;
            this.btnCreate.Text = "Добавить сотрудника";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Location = new System.Drawing.Point(712, 9);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(76, 23);
            this.btnSettings.TabIndex = 4;
            this.btnSettings.Text = "Настройки";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // LastName
            // 
            this.LastName.HeaderText = "Фамилия";
            this.LastName.Name = "LastName";
            this.LastName.ReadOnly = true;
            // 
            // FirstName
            // 
            this.FirstName.HeaderText = "Имя";
            this.FirstName.Name = "FirstName";
            this.FirstName.ReadOnly = true;
            // 
            // MiddleName
            // 
            this.MiddleName.HeaderText = "Отчество";
            this.MiddleName.Name = "MiddleName";
            this.MiddleName.ReadOnly = true;
            // 
            // LastQualification
            // 
            this.LastQualification.HeaderText = "Последняя аттестация";
            this.LastQualification.Name = "LastQualification";
            this.LastQualification.ReadOnly = true;
            // 
            // NextAttestation
            // 
            this.NextAttestation.HeaderText = "Следующая аттестация";
            this.NextAttestation.Name = "NextAttestation";
            this.NextAttestation.ReadOnly = true;
            // 
            // timeLeft
            // 
            this.timeLeft.HeaderText = "Осталось времени";
            this.timeLeft.Name = "timeLeft";
            this.timeLeft.ReadOnly = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.btnCertify);
            this.Controls.Add(this.dgvWorkers);
            this.Name = "FormMain";
            this.ShowIcon = false;
            this.Text = "Аттестация сотрудников";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvWorkers;
        private System.Windows.Forms.Button btnCertify;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MiddleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastQualification;
        private System.Windows.Forms.DataGridViewTextBoxColumn NextAttestation;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeLeft;
    }
}