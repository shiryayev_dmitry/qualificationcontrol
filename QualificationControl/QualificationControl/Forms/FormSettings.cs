﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QualificationControl
{
    public partial class FormSettings : Form
    {
        public FormSettings()
        {
            InitializeComponent();
            //Если стоить автоматическое выставление параметров
            if(Properties.Settings.Default.isSetTime)
            {
                //Установить текущие данные
                dateTimePicker1.Value = Properties.Settings.Default.sTime;
                textBox1.Text = Properties.Settings.Default.sYear.ToString();
                textBox2.Text = Properties.Settings.Default.sMonth.ToString();
                textBox3.Text = Properties.Settings.Default.sDay.ToString();
                radioButton1.Checked = true;
            }
            else
            {
                radioButton2.Checked = true;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Enabled = true;
            textBox2.Enabled = true;
            textBox3.Enabled = true;
            dateTimePicker1.Enabled = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            dateTimePicker1.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Сохранить параметры
            if(radioButton1.Checked)
            {
                Properties.Settings.Default.isSetTime = true;
                Properties.Settings.Default.sTime = dateTimePicker1.Value;
                Properties.Settings.Default.sYear = Int32.Parse(textBox1.Text);
                Properties.Settings.Default.sMonth = Int32.Parse(textBox2.Text);
                Properties.Settings.Default.sDay = Int32.Parse(textBox3.Text);
            }
            else
            {
                Properties.Settings.Default.isSetTime = false;
            }
            
            this.Close();
        }
    }
}
