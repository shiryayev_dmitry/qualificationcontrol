﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QualificationControl
{
    public partial class FormCreate : Form
    {
        Mdb mdb;
        public FormCreate(Mdb _mdb)
        {
            InitializeComponent();
            mdb = _mdb;
            if (Properties.Settings.Default.isSetTime)
            {
                int year = DateTime.Now.Year + Properties.Settings.Default.sYear;
                int month = DateTime.Now.Month + Properties.Settings.Default.sMonth;
                if (month > 12)
                {
                    while (month > 12)
                    {
                        year += 1;
                        month -= 12;
                    }
                }
                int days = DateTime.Now.Day + Properties.Settings.Default.sDay;
                if (days > DateTime.DaysInMonth(year, month))
                {
                    while (days > DateTime.DaysInMonth(year, month))
                    {
                        month += 1;
                        if (month > 12)
                        {
                            month -= 12;
                            year += 1;
                        }
                        days -= DateTime.DaysInMonth(year, month);
                    }
                }
                dateTimePicker2.Value = DateTime.Parse(string.Format("{0}.{1}.{2} {3}", year, month, days,
                    Properties.Settings.Default.sTime.ToLongTimeString()));
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Workers worker = new Workers();
            worker.LastName = tbLastName.Text;
            worker.MiddleName = tbMiddleName.Text;
            worker.FirstName = tbName.Text;
            worker.LastCertify = dateTimePicker1.Value;
            worker.NextCertify = dateTimePicker2.Value;
            
            mdb.DoCreateWorkers(worker);
            this.Close();
        }
    }
}
