﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QualificationControl
{
    public partial class FormsChange : Form
    {
        Mdb mdb;
        int id;
        public FormsChange(int _id, Mdb _mdb)
        {
            InitializeComponent();
            mdb = _mdb;
            id = _id;
            Workers worker = mdb.GetWorkerById(id);
            //Установить текущие параметры
            setActualInf(worker);
        }

        private void setActualInf(Workers worker)
        {
            tbLastName.Text = worker.LastName;
            tbMiddleName.Text = worker.MiddleName;
            tbName.Text = worker.FirstName;
            dateTimePicker1.Value = worker.LastCertify;
            dateTimePicker2.Value = worker.NextCertify;
        }
        //Сохранить все изменения
        private void btnSave_Click(object sender, EventArgs e)
        {
            Workers worker = new Workers();
            worker.Id = id;
            worker.LastName = tbLastName.Text;
            worker.MiddleName = tbMiddleName.Text;
            worker.FirstName = tbName.Text;
            worker.LastCertify = dateTimePicker1.Value;
            worker.NextCertify = dateTimePicker2.Value;
            mdb.DoChangeWorkerProps(worker);
            this.Close();
        }
    }
}
