﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QualificationControl
{
    public partial class FormMain : Form
    {
        Mdb mdb = new Mdb();
        Thread thread = null;
        bool update = true;
        /// <summary>
        /// Динамический timer
        /// </summary>
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        public FormMain()
        {
            InitializeComponent();
            //Подключение бд
            mdb.DoConnected("../../../Workers.mdb");
        }
        /// <summary>
        /// Создать работника
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreate_Click(object sender, EventArgs e)
        {
            FormCreate fCreate = new FormCreate(mdb);
            fCreate.ShowDialog();
            doUpdateTable();
        }
        /// <summary>
        /// Аттестовать сотрудника
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCertify_Click(object sender, EventArgs e)
        {
            //Если работник выбран
            if(dgvWorkers.CurrentRow != null)
            {
                //Получить id работника
                int fId = Int32.Parse(dgvWorkers.CurrentRow.Cells[0].Value.ToString());
                FormCertify fCertify = new FormCertify(fId, mdb);
                fCertify.ShowDialog();
                doUpdateTable();
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            //Загрузить таблицу с работниками
            doUpdateTable();
        }
        //Получить время до начала аттестациии
        private int GetWaitSeconds(DateTime nextSertify)
        {
            int hour = nextSertify.Hour - DateTime.Now.Hour;
            int minute = nextSertify.Minute - DateTime.Now.Minute;
            int sec = nextSertify.Second - DateTime.Now.Second;
            return sec + minute * 60 + hour * 3600;
        }
        //Установить время до окончания аттестации, если осталось меньше 1-го дня
        private void DoSetTime(Workers worker, int index)
        {
            //Получить оставшееся время
            int time = GetWaitSeconds(worker.NextCertify);
            if (time > 0)
            {
                while (time > 0)
                {
                    if (update)
                    {
                        TimeSpan t = TimeSpan.FromSeconds(time);
                        dgvWorkers.Rows[index].Cells["timeLeft"].Value = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                t.Hours,
                                t.Minutes,
                                t.Seconds);
                        Thread.Sleep(1000);
                        time -= 1;
                    }
                    else
                    {
                        time = 0;
                    }
                }
            }
            else
            {
                dgvWorkers.Rows[index].Cells["timeLeft"].Value = "Просрочено";
                dgvWorkers.Rows[index].DefaultCellStyle.BackColor = Color.Red;
                MessageBox.Show("Аттестация просрочена! id: " + worker.Id);
            }
            
        }
        /// <summary>
        /// Обновить таблицу
        /// </summary>
        private void doUpdateTable()
        {
            List<Workers> workers = mdb.GetWorkersList();
            dgvWorkers.Rows.Clear();
            int index = 0;
            update = false;
            if (thread != null)
            {
                thread.Abort();
            }
            update = false;
            Thread.Sleep(1000);
            foreach (Workers worker in workers)
            {
                dgvWorkers.Rows.Add();
                dgvWorkers.Rows[index].Cells["ID"].Value = worker.Id;
                dgvWorkers.Rows[index].Cells["LastName"].Value = worker.LastName;
                dgvWorkers.Rows[index].Cells["FirstName"].Value = worker.FirstName;
                dgvWorkers.Rows[index].Cells["MiddleName"].Value = worker.MiddleName;
                dgvWorkers.Rows[index].Cells["LastQualification"].Value = worker.LastCertify.ToShortDateString();
                dgvWorkers.Rows[index].Cells["NextAttestation"].Value = worker.NextCertify.ToString("G");
                if(worker.NextCertify.ToShortDateString() == DateTime.Now.ToShortDateString())
                {
                    update = true;
                    int id = index;
                    Task.Factory.StartNew(() => {
                        thread = Thread.CurrentThread;
                        DoSetTime(worker, id);
                        });
                }
                else
                {
                    //Получить количество дней, месяцев и лет до окончания аттестации
                    int years = worker.NextCertify.Year - DateTime.Now.Year;
                    int monthes = worker.NextCertify.Month - DateTime.Now.Month;
                    if(monthes<0)
                    {
                        while(monthes<0)
                        {
                            years -= 1;
                            monthes += 12;
                        }
                    }
                    int days = worker.NextCertify.Day - DateTime.Now.Day;
                    if(days<0)
                    {
                        while(days<0)
                        {
                            monthes -= 1;
                            if (monthes < 0)
                            {
                                monthes += 12;
                                years -= 1;
                            }
                            if(years > 0)
                            {
                                days += DateTime.DaysInMonth(years, monthes);
                            }
                            else
                            {
                                days = 0;
                            }
                        }
                    }

                    if(years < 0)
                    {
                        dgvWorkers.Rows[index].Cells["timeLeft"].Value = "Просрочено";
                        dgvWorkers.Rows[index].DefaultCellStyle.BackColor = Color.Red;
                        MessageBox.Show("Аттестация просрочена! id: " + worker.Id);
                    }
                    else
                    {
                        dgvWorkers.Rows[index].Cells["timeLeft"].Value = string.Format("{0}л {1}м {2}д",
                            years, monthes, days
                            );
                    }

                }
                index += 1;
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            FormSettings fSettings = new FormSettings();
            fSettings.ShowDialog();
        }

        private void dgvWorkers_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvWorkers.CurrentRow != null)
            {
                //Открыть окно изменения информации по работнику
                int fId = Int32.Parse(dgvWorkers.CurrentRow.Cells[0].Value.ToString());
                FormsChange fChange = new FormsChange(fId, mdb);
                fChange.ShowDialog();
                doUpdateTable();
            }
        }
    }
}
