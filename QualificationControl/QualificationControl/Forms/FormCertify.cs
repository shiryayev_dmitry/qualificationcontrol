﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QualificationControl
{
    public partial class FormCertify : Form
    {
        int id;
        Mdb mdb;
        public FormCertify(int _id, Mdb _mdb)
        {
            InitializeComponent();
            id = _id;
            mdb = _mdb;
            //Если стоит автоматическое задание даты аттестации
            if (Properties.Settings.Default.isSetTime)
            {
                //Установить дату и время следующей аттестации
                int year = DateTime.Now.Year + Properties.Settings.Default.sYear;
                int month = DateTime.Now.Month + Properties.Settings.Default.sMonth;
                if (month > 12)
                {
                    while (month > 12)
                    {
                        year += 1;
                        month -= 12;
                    }
                }
                int days = DateTime.Now.Day + Properties.Settings.Default.sDay;
                if (days > DateTime.DaysInMonth(year, month))
                {
                    while (days > DateTime.DaysInMonth(year, month))
                    {
                        month += 1;
                        if (month > 12)
                        {
                            month -= 12;
                            year += 1;
                        }
                        days -= DateTime.DaysInMonth(year, month);
                    }
                }
                dateTimePicker2.Value = DateTime.Parse(string.Format("{0}.{1}.{2} {3}", year, month, days,
                    Properties.Settings.Default.sTime.ToLongTimeString()));
            }
            else
            {
                //Иначе задать текущее время
                dateTimePicker1.Value = DateTime.Now;
                dateTimePicker2.Value = DateTime.Now;
            }
            
        }
        /// <summary>
        /// Сохранить данные
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            mdb.DoCertifited(id, dateTimePicker1.Value, dateTimePicker2.Value);
            this.Close();
        }
    }
}
